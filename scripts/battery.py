#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import fontawesome as fa

def status(id):
	bat_id = id
	bat_color = ['#ffffff', '#ffffff']

	bat_status = ''

	battery_empty = fa.icons['battery-empty']
	battery_quarter = fa.icons['battery-quarter']
	battery_half = fa.icons['battery-half']
	battery_three_quarters = fa.icons['battery-three-quarters']
	battery_full = fa.icons['battery-full']
	battery_bolt = fa.icons['bolt']

	bat_file_full = open('/sys/class/power_supply/BAT%s/charge_full' % bat_id, 'r')
	bat_full = int(bat_file_full.read())
	bat_file_now = open('/sys/class/power_supply/BAT%s/charge_now' % bat_id, 'r')
	bat_now = int(bat_file_now.read())
	ac_file_now = open('/sys/class/power_supply/AC/online', 'r')
	ac_now = int(ac_file_now.read())
	
	bat_percent = int(bat_now * 100 / bat_full)

	bat_heart = int((bat_percent + 1)/25)

	if ac_now == 1:
		bat_status = battery_bolt
	elif bat_heart == 4:
		bat_status = battery_full
	elif bat_heart == 3:
		bat_status = battery_three_quarters
	elif bat_heart == 2:
		bat_status = battery_half
	elif bat_heart == 1:
		bat_status = battery_quarter
	elif bat_heart == 0:
		bat_status = battery_empty

	return(json.loads('{"name": "battery", "full_text": "%s %s%%", "color": "%s" }' %
		(bat_status, bat_percent, bat_color[bat_id])))
