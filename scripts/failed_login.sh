#!/bin/bash

# Edit the /etc/pam.d/common-auth file and insert this line immediately before the line with pam_deny.so module
# auth    [default=ignore]                pam_exec.so seteuid /home/rcastagnet/.config/i3/scripts/failed_login.sh
# Now edit the two lines above (pam_unix and pam_sss) and change the success=2 to success=3 and likewise success=1 to success=2. This has it skip an extra line when auth is successful. So it skips our script.

now=$(date +%Y%m%d_%H%M%S)
fswebcam ~/failed_login/failed_$now.jpg
exit 0
