#!/bin/bash

## service au démarrage
sleep 1
#nohup parcellite &
nohup xautolock -time 10 -locker 'i3lock -c 000000 -d' &
setxkbmap -layout fr-latin9
/home/adaurelle/.config/i3/scripts/randr_1.sh
i3-msg restart && touch ~/toto
nohup redshift -l 44.85777:-0.56020 &
nohup sudo usbguard-applet-qt &
