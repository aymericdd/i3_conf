#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import logging
import fontawesome as fa
import docker
DOCKER_CLIENT = docker.DockerClient(base_url='unix://var/run/docker.sock')
RUNNING = 'running'

def _is_running(container_name):
    """
    verify the status of a sniffer container by it's name
    :param container_name: the name of the container
    :return: Boolean if the status is ok
    """
    try:
        container = DOCKER_CLIENT.containers.get(container_name)
    except Exception:
        return False

    container_state = container.attrs['State']

    container_is_running = container_state['Status'] == RUNNING

    vpn_ready_log_message = "Setting up DNS resolvers"

    if vpn_ready_log_message not in container.logs(tail=1):
        return False
    
    return container_is_running

def status(id):
    my_container_name = "nsgclient"
    vpn_enable_icon = fa.icons['toggle-on']
    vpn_disable_icon = fa.icons['toggle-off']
    vpn_status_icon = vpn_enable_icon if _is_running(my_container_name) else vpn_disable_icon

    vpn_id = id
    vpn_enable_color = '#69ce54'
    vpn_disable_color = '#f21313'
    vpn_status_color = vpn_enable_color if _is_running(my_container_name) else vpn_disable_color

    
    return(json.loads('{"name": "vpn", "full_text": "VPN %s", "color": "%s" }' %
	(vpn_status_icon, vpn_status_color)))
